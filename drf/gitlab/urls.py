from django.urls import path, include
from . import views


urlpatterns = [
    path("projects/", views.ProjectsView.as_view(), name = 'projects'),
    path("projects/<str:id>/", views.ProjectsChange.as_view(), name = 'projects_id'),
    path("users/", views.UsersView.as_view(), name = 'users'),
    path("merges/", views.MergesView.as_view(), name = 'merges'),
]