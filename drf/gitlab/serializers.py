from rest_framework import serializers

from .models import Project, Users, Merges


class ProjectGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        exclude = ("token", )

class ProjectPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = "__all__"

class UsersGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Users
        fields = "__all__"

class MergesGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Merges
        fields = "__all__"




