from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Project, Users, Merges
from .serializers import ProjectGetSerializer, ProjectPostSerializer, UsersGetSerializer, MergesGetSerializer

from django.http import HttpResponseRedirect
from django.urls import reverse_lazy


class ProjectsView(APIView):
    def get(self, request):
        projects = Project.objects.all()
        serializer = ProjectGetSerializer(projects, many=True)
        return Response(serializer.data)
    def post(self,request):
        review = ProjectPostSerializer(data = request.data)
        if review.is_valid():
            review.save()
#            add_users(request.data.get('project_id'))
        return HttpResponseRedirect(reverse_lazy('projects'))

class ProjectsChange(APIView):
    def get(self, request,id):
        projects = Project.objects.filter(project_id=id)
        serializer = ProjectGetSerializer(projects, many=True)
        return Response(serializer.data)
    def put(self,request,id):
        if request.data.get('token') != None:
            Project.objects.filter(project_id=id).update(token=request.data.get('token'))
        return HttpResponseRedirect(reverse_lazy('projects')+"id/")
    def delete(self,request,id):
        if request.data.get('project_id') != None:
            Project.objects.filter(project_id=id).delete()
        return HttpResponseRedirect(reverse_lazy('projects'))

class UsersView(APIView):
    def get(self, request):
        projects = Users.objects.all()
        serializer = UsersGetSerializer(projects, many=True)
        return Response(serializer.data)
    def put(self,request):
        if request.data.get('name_user') != None:
            if request.data.get('telegram') != None:
                Users.objects.filter(name_user=request.data.get('name_user')).update(telegram=request.data.get('telegram'))
            if request.data.get('status') != None:
                Users.objects.filter(name_user=request.data.get('name_user')).update(status=request.data.get('status'))
        return HttpResponseRedirect(reverse_lazy('users'))

class MergesView(APIView):
    def get(self, request):
        projects = Merges.objects.all()
        serializer = MergesGetSerializer(projects, many=True)
        return Response(serializer.data)



