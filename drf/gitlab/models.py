from django.db import models

class Project(models.Model):
    project_id = models.CharField(max_length=100, primary_key=True)
    admin =models.CharField(max_length=100)
    token = models.CharField(max_length=100)

class Users(models.Model):
    name_user = models.CharField(max_length=100)
    telegram = models.CharField(max_length=100, default="")
    project_id = models.CharField(max_length=100)
    merge_id = models.CharField(max_length=100, default="")
    status = models.CharField(max_length=100, default="Работает")
#    time_work = models.DateField(default = "2015-10-15 07:49:18")
class Merges(models.Model):
    name_user = models.CharField(max_length=100)
    project_id = models.CharField(max_length=100)
    merge_id = models.CharField(max_length=100)
#    time_merged  = models.DateField(max_length=100)
